#!/usr/bin/env bash

cd /tmp
rm -rf flat-remix
echo 'Download source code'
git clone --depth 1 git@github.com:daniruiz/flat-remix.git
mkdir -p ~/.icons

echo 'Copy files.'
cp -r flat-remix/Flat-Remix* ~/.icons/

echo 'Update setting.'
gsettings set org.gnome.desktop.interface icon-theme "Flat-Remix"


