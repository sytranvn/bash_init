#!/usr/bin/env bash

function checkCommand()
{
    local __command=$1
    command -v $1 >/dev/null 2>&1 || { echo >&2 "Require $1 but it's not installed.  Aborting."; exit 1; }
}

function checkInstallCommand()
{
    local __command=$1
    command -v $1 >/dev/null 2>&1 || { echo >&2 "Require $1 but it's not installed.  Installing."; sudo apt-get install -y $1; }
}

echo 'Moving into /tmp folder'
cd /tmp

echo 'Cloning adapta repository'
git clone --depth 1 git@github.com:adapta-project/adapta-gtk-theme.git

# Install requried apps
checkInstallCommand sassc
checkInstallCommand inkscape

# Install
cd adapta-gtk-theme

echo 'Build resouce.'
read -p 'Do you want to install adapta system-wide? [Y/n]:' yn
yn=${yn:-Y}

if [[ $yn = "Y" || $yn = "y" ]]; then
    ./autogen.sh --prefix=/usr
else
    ./autogen.sh
fi

echo 'Installing '
make && sudo make install
