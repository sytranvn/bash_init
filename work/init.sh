#!/usr/bin/env bash
INITS=$(find . -name init.sh)
YES=true && [[ ! "$1" == '-y' ]] && YES=false

for INIT in $INITS
do
    if [ ! -x $INIT ]; then
        chmod +x $INIT
    fi
    if $YES; then
        $INIT
    else
        read -p "Run $INIT? [Y/n]" yn
        : ${yn:='Y'}
        if [[ "$yn" == 'Y' ]] || [[ "$yn" == 'y' ]]; then
            $INIT
        fi
    fi
done

echo 'Complete!'

