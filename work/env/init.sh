#!/usr/bin/env bash
sudo apt-get install direnv
[[ -z $(grep 'eval "$(direnv hook bash)"' ~/.bashrc) ]] && echo 'eval "$(direnv hook bash)"' >> ~/.bashrc
echo 'Install complete. Please reload terminal.'

