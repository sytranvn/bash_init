 # Docker aliases
 alias docker-stop-all="docker stop $(docker ps -a -q)"
 alias docker-rm-all="docker-stop-all && docker rm $(docker ps -a -q)"
 
 # Unified aliases
 alias bastion='ssh sytran@bastion.unified.com'
 
 _cdUnf() {
     local dirs cur
     cur="${COMP_WORDS[COMP_CWORD]}"
     if [[ -z $cur ]]; then
         dirs=( $(find $UNIFIED_HOME -maxdepth 1 -mindepth 1 -type d ) )
     else
         dirs=( $(find $UNIFIED_HOME -maxdepth 1 -mindepth 1 -type d -name "$cur*") )
     fi
     COMPREPLY=(${dirs[@]##*/})
     return 0
 }
 
 cdUnf() {
     cd "$UNIFIED_HOME/$1"
 }
 alias unf=cdUnf
 complete -F _cdUnf unf

