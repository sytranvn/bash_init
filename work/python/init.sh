#!/usr/bin/env bash
C_DIR=${PWD##*/}
SELECT=$1
set -e

####################################################################################
# ./pycom.sh [version]
# Go to python project folder and run command to install virtual environment and
# packages. Default version to install is 3.4.9
#
####################################################################################

function setbrc (){
    echo $1 >> $HOME/.bashrc
}

if ! command -v pyenv 1>/dev/null 2>&1 ; then
        curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash 1>/dev/null 2>&1
        setbrc '# Pyenv variables'
        export PYENV_ROOT="$HOME/.pyenv"
        setbrc "export PATH=\"${PYENV_ROOT}/bin:\$PATH\""
        setbrc "eval \"\$(pyenv init -)\""
        setbrc "eval \"\$(pyenv virtualenv-init -)\""

        export PATH="$PYENV_ROOT/bin:$PATH"
        eval "$(pyenv init -)"
        eval "$(pyenv virtualenv-init -)"
fi
if [ -z $SELECT ]; then
    PY_VER=$(pyenv versions | grep -oP -m 1 '3\.4\.\d+' || echo)
else
    PY_VER=$(pyenv versions | grep -o -m 1 $SELECT || echo)
fi

if [ -z $PY_VER ]; then
        PY_VER=${SELECT:='3.4.9'}
        pyenv install $PY_VER
        #########################################################################################
        # https://github.com/pyenv/pyenv/wiki/common-build-problems
        # If build fail try install following packages.
        #
        # sudo apt-get install -y make build-essential libssl1.0-dev zlib1g-dev libbz2-dev \
        # libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
        # xz-utils tk-dev libffi-dev liblzma-dev python-openssl
        #########################################################################################
fi

if [ -f requirements.txt ]; then
    if ! pyenv virtualenvs | grep "$PY_VER/envs/$C_DIR"; then
        pyenv virtualenv "$PY_VER" "$C_DIR" > /dev/null
        echo "Virtual environment $PY_VER/envs/$C_DIR created."
    fi

    eval "$(pyenv init -)"
    pyenv activate "$PY_VER/envs/$C_DIR"

    echo 'Install requirements package.'
    PKS=$(grep -P '^\w' requirements.txt | grep -v 'psycopg2')
    for PK in $PKS;
    do
        pip --disable-pip-version-check install $PK > /dev/null
    done
    pip --disable-pip-version-check install 'psycopg2>=2.7' > /dev/null
    echo 'Completed ^_^'
    exit 0
else
    echo 'Please goto project directory.'
fi

exit 1
